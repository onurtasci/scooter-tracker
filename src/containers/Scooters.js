import React, { useState, useEffect } from 'react'
import Map from '../components/Map'
import Circle from '../components/Circle'
import scooterList from '../data/scooters-data'
import cityList from '../data/cities-data'
import speeds from '../data/speed-list'
import SideBar from '../components/SideBar'

const Scooters = props => {
    const [selectedCity, setSelectedCity] = useState(cityList[0])
    const [latIncrement, setLatIncrement] = useState(0)
    const [lngIncrement, setLngIncrement] = useState(0)
    const [speedList, setSpeedList] = useState(speeds)

    useEffect(() => {
        let interval = null;
        interval = setInterval(() => {
            setLngIncrement(lngIncrement => lngIncrement + updateCoord())
            setLatIncrement(latIncrement => latIncrement + updateCoord())

            let arr = [...speedList]
            for (const key in speedList) {
                arr[key].lat = updateCoord()
                arr[key].lng = updateCoord()
            }
            setSpeedList(arr)

        }, 1000)
        return () => clearInterval(interval)
    }, [latIncrement, lngIncrement])

    return (
        <div>
            <Map center={selectedCity.center} >
                {scooterList.filter(scooter => scooter.city === selectedCity.value).map((sc, i) => (
                    <Circle
                        key={sc.id}
                        lat={sc.lat + speedList[i].lat}
                        lng={sc.lng + speedList[i].lng}
                    />
                ))}
            </Map>
            <SideBar
                onClick={(city) => { setSelectedCity(city) }}
                activeCity={selectedCity.value}
            />
        </div>
    )
}

const updateCoord = () => {
    let change = Math.random() * 0.001
    // change *= Math.floor(Math.random() * 2) === 1 ? 1 : -1
    return change
}

export default Scooters