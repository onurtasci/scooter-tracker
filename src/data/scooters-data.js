const scooters = [
    {id: '01', city: 'odessa', lat: 46.4815, lng: 30.7333},
    {id: '02', city: 'odessa', lat: 46.4895, lng: 30.7233},
    {id: '03', city: 'odessa', lat: 46.4575, lng: 30.7133},
    {id: '04', city: 'odessa', lat: 46.4925, lng: 30.7288},
    {id: '05', city: 'odessa', lat: 46.4625, lng: 30.7211},
    {id: '06', city: 'odessa', lat: 46.4815, lng: 30.7599},
    {id: '07', city: 'odessa', lat: 46.4895, lng: 30.7166},
    {id: '08', city: 'odessa', lat: 46.4575, lng: 30.7247},
    {id: '09', city: 'odessa', lat: 46.4925, lng: 30.7298},
    {id: '10', city: 'odessa', lat: 46.4625, lng: 30.7261},


    {id: '11', city: 'kiev', lat: 50.4401, lng: 30.5214},
    {id: '12', city: 'kiev', lat: 50.4501, lng: 30.5364},
    {id: '13', city: 'kiev', lat: 50.4601, lng: 30.5454},
    {id: '14', city: 'kiev', lat: 50.4401, lng: 30.5394},
    {id: '15', city: 'kiev', lat: 50.4501, lng: 30.5274},
    {id: '16', city: 'kiev', lat: 50.4601, lng: 30.5324},
    {id: '17', city: 'kiev', lat: 50.4501, lng: 30.5494},
    {id: '18', city: 'kiev', lat: 50.4701, lng: 30.5114},
    {id: '19', city: 'kiev', lat: 50.4401, lng: 30.5174},
    {id: '20', city: 'kiev', lat: 50.4601, lng: 30.5254},


    {id: '21', city: 'lviv', lat: 49.8297, lng: 24.0297},
    {id: '22', city: 'lviv', lat: 49.8337, lng: 24.0387},
    {id: '23', city: 'lviv', lat: 49.8447, lng: 24.0177},
    {id: '24', city: 'lviv', lat: 49.8577, lng: 24.0067},
    {id: '25', city: 'lviv', lat: 49.8117, lng: 24.0457},
    {id: '26', city: 'lviv', lat: 49.8327, lng: 24.0547},
    {id: '27', city: 'lviv', lat: 49.8237, lng: 24.0337},
    {id: '28', city: 'lviv', lat: 49.8477, lng: 24.0227},
    {id: '29', city: 'lviv', lat: 49.8067, lng: 24.0197},
    {id: '30', city: 'lviv', lat: 49.8157, lng: 24.0017},
]

export default scooters