const Cities = [
    {
        id: '01',
        label: 'ODESSA SAFE ZONE',
        value: 'odessa',
        center: {
            lat: 46.4825,
            lng: 30.7233
        }
    },
    {
        id: '02',
        label: 'KIEV SAFE ZONE',
        value: 'kiev',
        center: {
            lat: 50.4501,
            lng: 30.5234
        }
    },
    {
        id: '03',
        label: 'LVIV SAFE ZONE',
        value: 'lviv',
        center: {
            lat: 49.8397,
            lng: 24.0297
        }
    },
]

export default Cities