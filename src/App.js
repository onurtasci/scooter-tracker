import React from 'react';
import Scooters from './containers/Scooters'

function App() {
  return (
    <div className="App">
      <Scooters/>
    </div>
  );
}

export default App;
