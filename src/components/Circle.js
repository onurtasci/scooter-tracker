import React from 'react'
import Pin from '../assets/images/pin.png'

const Circle = props => {
    return (
        <img src={Pin} alt='kiwi-pin' />
    )
}

export default Circle