import React from 'react'
import Logo from './Logo'
import citiesList from '../data/cities-data'

const SideBar = props => {
    return (
        <div style={{
            width: '320px',
            height: '100%',
            backgroundColor: 'white',
            position: 'absolute',
            left: 0,
            top: 0,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            paddingTop: '30px',
            borderRight: '1px solid #ccc'
        }} >
            <Logo />
            {citiesList.map(c => (
                <button
                    key={c.id}
                    onClick={() => props.onClick(c)}
                    style={{
                        fontSize: 18,
                        color: props.activeCity === c.value ? '#13C93F' : '#073F15',
                        textDecorationLine: props.activeCity === c.value ? 'underline' : 'none',
                        marginBottom: '27px',
                        backgroundColor: 'transparent',
                        border: 'none',
                        outline: 'none',
                        cursor: 'pointer',
                        font: 'inherit',
                    }}
                >
                    {c.label}
                </button>
            ))}
        </div>
    )
}

export default SideBar