import React from 'react';
import GoogleMapReact from 'google-map-react';
import mapStyles from './MapStyles'
import googleMapApi from '../api';

const Map = props => {
    const zoom = 13

    return (
        <div style={{ height: '100vh', width: '100%' }}>
            <GoogleMapReact
                options={{ styles: mapStyles }}
                bootstrapURLKeys={{ key: googleMapApi }}
                center={props.center}
                defaultZoom={zoom}
            >
                {props.children}
            </GoogleMapReact>
        </div>
    );
}

export default Map;