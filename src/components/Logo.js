import React from 'react'
import Logo from '../assets/images/logo.png'

const logo = props => (
    <div style={{
        width: 163.47,
        height: 66.13,
        marginBottom: 56
    }}>
        <img src={Logo} alt={'kiwi-logo'} />
    </div>
)

export default logo