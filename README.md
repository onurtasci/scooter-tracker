## Scooter Tracker App

Add your google map api key to src/api.js folder to see the map with a grey skin.

![alt text](https://i.ibb.co/LvqH0m4/kiwi.png)

In the project directory, you can run:

### `npm install`

Installs the node_modules folder to the project

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.
